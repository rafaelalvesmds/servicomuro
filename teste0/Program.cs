﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static teste0.Comp.Composicoes;
using static teste0.Comp.Insumos.Insumos;
using System.Data.SqlClient;

namespace teste0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Digite um UF: ");
            string input_uf = Console.ReadLine();


            Capacete capacete = new Capacete(1, "x01", "Capacete", 0.17, "conjunto", 40, "AC");
            ProtetorSolarFrasco1L protetorSolarFrasco1L = new ProtetorSolarFrasco1L(2, "x02", "Protetor", 1, "unidade", 5, "AC");
            Botina botina = new Botina(3, "x03", "Botina", 0.34, "par", 25, "AC");
            Uniforme uniforme = new Uniforme(4, "x04", "Uniforme", 0.17, "conjunto", 100, "AC");
            PedreiroMensalista pedreiroMensalista = new PedreiroMensalista(5, "a012", "PedreiroMensalista", 0.0052, "mês", 1000, "AC");
            Alimentacao alimentacao = new Alimentacao(6, "b01", "Alimentacao", 20, "mês", 12, "AC");
            Transporte transporte = new Transporte(7, "b02", "Transporte", 40, "mês", 5, "AC");
            ArgamassaPreparoManual argamassaPreparo = new ArgamassaPreparoManual(8, "c893", "ArgamassaPreparoManual", 12, "kg", 7, "AC");
            Blocos12cm blocos12 = new Blocos12cm(9, "m453", "Blocos12cm", 20, "unidade", 0.16, "AC");

            Epi epi = new Epi(capacete, protetorSolarFrasco1L, botina, uniforme, input_uf);
            PedreiroComEncargos pedreiroComEncargos = new PedreiroComEncargos(pedreiroMensalista, alimentacao, transporte, epi, input_uf);
            Muro muro = new Muro(pedreiroComEncargos, argamassaPreparo, blocos12, input_uf);

            var valorEpi = epi.ValorEpi(input_uf);
            var valorPedreiro = pedreiroComEncargos.ValorPedreiro(input_uf);
            var valorMuro = muro.ValorMuro(input_uf);

            Console.WriteLine($"O valor do Epi é: R$ {valorEpi}");
            Console.WriteLine($"O valor de Pedreiro com Encargos é: R$ {valorPedreiro}");
            Console.WriteLine($"O valor para realizar o serviço muro é igual a : R$ {valorMuro}");
            Console.ReadLine();

        }
    }
}
