﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using teste0.Comp.Insumos;
using static teste0.Comp.Insumos.Insumos;
using teste0;

namespace teste0.Comp
{
    class Composicoes
    {
        public class Epi
        {
            public Capacete Capacete { get; set; }
            public ProtetorSolarFrasco1L ProtetorSolarFrasco1L { get; set; }
            public Botina Botina { get; set; }
            public Uniforme Uniforme { get; set; }
            public string UfEpi { get; set; }


            public Epi(Capacete capacete, ProtetorSolarFrasco1L protetorSolarFrasco1L, Botina botina, Uniforme uniforme, string ufEpi)
            {
                Capacete = capacete;
                ProtetorSolarFrasco1L = protetorSolarFrasco1L;
                Botina = botina;
                Uniforme = uniforme;
                UfEpi = ufEpi;
            }
            public double ValorEpi(string ufEpi)
            {
                if (Capacete.Uf == ufEpi)
                {
                    double precoCapacete = Capacete.Preco;
                    double quantidadeCapacete = Capacete.Qtde;
                    double precoProtetor = ProtetorSolarFrasco1L.Preco;
                    double quantidadeProtetor = ProtetorSolarFrasco1L.Qtde;
                    double precoBotina = Botina.Preco;
                    double quantidadeBotina = Botina.Qtde;
                    double precoUniforme = Uniforme.Preco;
                    double quantidadeUniforme = Uniforme.Qtde;

                    double total = (precoCapacete * quantidadeCapacete) + (precoProtetor * quantidadeProtetor) + (precoBotina * quantidadeBotina) + (precoUniforme * quantidadeUniforme);
                    return total;
                } else
                {
                    return 0;
                }
            }
        }

        public class PedreiroComEncargos
        {
            public PedreiroMensalista PedreiroMensalista { get; set; }
            public Alimentacao Alimentacao { get; set; }
            public Transporte Transporte { get; set; }
            public Epi Epi { get; set; }
            public string UfPedreiro { get; set; }

            public PedreiroComEncargos(PedreiroMensalista pedreiroMensalista, Alimentacao alimentacao, Transporte transporte, Epi epi, string ufPedreiro)
            {
                PedreiroMensalista = pedreiroMensalista;
                Alimentacao = alimentacao;
                Transporte = transporte;
                Epi = epi;
                UfPedreiro = ufPedreiro;
            }

            public double ValorPedreiro(string UfPedreiro)
            {
                
                if(PedreiroMensalista.Uf == UfPedreiro)
                {
                    double precoPedreiro = PedreiroMensalista.Preco;
                    double quantidadePedreiro = PedreiroMensalista.Qtde;
                    double precoAlimentacao = Alimentacao.Preco;
                    double quantidadeAlimentacao = Alimentacao.Qtde;
                    double precoTransporte = Transporte.Preco;
                    double quantidadeTransporte = Transporte.Qtde;
                    double precoEpi = Epi.ValorEpi(UfPedreiro);
                    
                    double total = (precoPedreiro * quantidadePedreiro) + (precoAlimentacao * quantidadeAlimentacao) + (precoTransporte * quantidadeTransporte) + precoEpi;
                    return total;
                }
                return 0;
            }
        }

        public class Muro
        {
            public PedreiroComEncargos PedreiroComEncargos { get; set; }
            public ArgamassaPreparoManual ArgamassaPreparoManual { get; set; }
            public Blocos12cm Blocos12cm { get; set; }
            public string UfMuro { get; set; }

            public Muro(PedreiroComEncargos pedreiroComEncargos, ArgamassaPreparoManual argamassaPreparoManual, Blocos12cm blocos12cm, string ufMuro)
            {
                PedreiroComEncargos = pedreiroComEncargos;
                ArgamassaPreparoManual = argamassaPreparoManual;
                Blocos12cm = blocos12cm;
                UfMuro = ufMuro;
            }

            public double ValorMuro(string UfMuro)
            {
                if(ArgamassaPreparoManual.Uf == UfMuro)
                {
                    double precoPedreiroComEncargos = PedreiroComEncargos.ValorPedreiro(UfMuro);
                    double quantidadePedreiro = 0.5;
                    double precoArgamassa = ArgamassaPreparoManual.Preco;
                    double quantidadeArgamassa = ArgamassaPreparoManual.Qtde;
                    double precoBlocos = Blocos12cm.Preco;
                    double quantidadeBlocos = Blocos12cm.Qtde;

                    double total = (precoArgamassa * quantidadeArgamassa) + (precoBlocos * quantidadeBlocos) + (precoPedreiroComEncargos*quantidadePedreiro);
                    return total; 
                }
                return 0;
            }
        }
    }
}
