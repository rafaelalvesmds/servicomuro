﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace teste0.Comp.Insumos
{
    class Insumos
    {
        public class Capacete
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }

            public Capacete(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class ProtetorSolarFrasco1L
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }

            public ProtetorSolarFrasco1L(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class Botina
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }

            public Botina(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }
        public class Uniforme
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }


            public Uniforme(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class PedreiroMensalista
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }


            public PedreiroMensalista(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class Alimentacao
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }

            public Alimentacao(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }
        public class Transporte
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }


            public Transporte(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class ArgamassaPreparoManual
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }


            public ArgamassaPreparoManual(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }

        public class Blocos12cm
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public double Qtde { get; set; }
            public string Unidade { get; set; }
            public double Preco { get; set; }
            public string Uf { get; set; }

            public Blocos12cm(int id, string codigo, string descricao, double qtde, string unidade, double preco, string uf)
            {
                Id = id;
                Codigo = codigo;
                Descricao = descricao;
                Qtde = qtde;
                Unidade = unidade;
                Preco = preco;
                Uf = uf;
            }
        }


    }
}

